

class GraphSearch:

    def __init__(self, framework, remove_choice):

        self.frontier = [framework.initial_states]
        self.explored = []
        self.framework = framework
        self.remove_choice = remove_choice
        self.final_path = []
        self.start()


    def start(self):
        while True:
            if len(self.frontier):
                path = self.remove_choice(self.frontier, self)
                s = path[-1]
                self.explored.append(s)

                if self.framework.goal_test(s):
                    self.final_path.append(path)
                    return path

                for a in self.framework.actions(s):
                    result = self.framework.result(s, a)

                    if result[0] not in self.explored:
                        new_path = []
                        new_path.extend(path)
                        new_path.extend(self.framework.result(s, a))
                        if new_path not in self.frontier:
                            self.frontier.append(new_path)
            else:
                return False
