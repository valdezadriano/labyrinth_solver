import math


def dfs_choice(frontier, graph=None, heuristic=None):
    if any(isinstance(i, list) for i in frontier[0]):
        choice = frontier[-1]
        frontier.remove(frontier[-1])
    else:
        choice = [frontier[-1]]
        [frontier.remove(frontier[-1])]
    return choice


def bfs_choice(frontier, graph=None, heuristic=None):
    if any(isinstance(i, list) for i in frontier[0]):
        choice = frontier[0]
        frontier.remove(frontier[0])
    else:
        choice = [frontier[0]]
        [frontier.remove(frontier[0])]
    return choice


def a_star(frontier, graph, heuristic):
    heuristics = [heuristic(frontier, graph)]
    choice = frontier[heuristics]
    frontier.remove(frontier[heuristics])
    return choice


def heuristic_1(node, graph):
    #manhattan with tie-breaker
    while type(node[-1]) != int:
        node = node[-1]
    heuristics = []
    for x in graph.framework.goal_states:
        dx1 = abs(node[1] - x[1])
        dy1 = abs(node[0] - x[0])
        dx2 = abs(graph.framework.initial_states[1] - x[1])
        dy2 = abs(graph.framework.initial_states[0] - x[0])
        cross = abs(dx1*dy2 - dx2*dy1)
        heuristics.append(cross * 0.001)

    return min(heuristics)


def heuristic_2(node, graph):
    while type(node[-1]) != int:
        node = node[-1]
    heuristics = []
    for x in graph.framework.goal_states:
        dx = abs(node[1] - x[1])
        dy = abs(node[0] - x[0])
        heuristics.append(math.sqrt(dx * dx + dy * dy))

    return min(heuristics)
