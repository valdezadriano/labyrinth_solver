import shutil
import os

from image_reader import ImageRead
from formal_problem import FormalProblem
from graph_search import GraphSearch
from image_reader import remake_image
import algorithms

size = 100
image_name = "4.bmp"
shutil.rmtree("./images/sliced_images")
os.makedirs("./images/sliced_images")
shutil.copy("{}{}".format(".\images\entry-image\ ".strip(), image_name), "./images/sliced_images/")
image = ImageRead(size, image_name)
image.discretize()
image.join_images()
framework = FormalProblem(image.discretized_matrix)
framework1 = framework


dfs_graph = GraphSearch(framework, lambda frontier, graph: algorithms.dfs_choice(frontier))
#bfs_graph = GraphSearch(framework, lambda frontier, graph: algorithms.bfs_choice(frontier))
#a_star_1_graph = GraphSearch(framework, lambda frontier, graph:
#algorithms.bfs_choice(frontier, graph, algorithms.heuristic_1(frontier, graph)))
#a_star_2_graph = GraphSearch(framework, lambda frontier, graph:
#algorithms.bfs_choice(frontier, graph, algorithms.heuristic_2(frontier, graph)))
shutil.rmtree("./images/final-image")
os.makedirs("./images/final-image")
print(dfs_graph.explored)
try:
    remake_image("{}{}".format(".\images\discretized\ ".strip(), image_name),
             "{}{}{}".format("./images/final-image/dfs-node-", 1, "-image.png"), size, dfs_graph.final_path)
except:
    print("no solution")
#print(dfs_graph.framework.explored)

#try:
    #remake_image("{}{}".format(".\images\discretized\ ".strip(), image_name),
            # "{}{}{}".format("./images/final-image/bfs-node-", 1, "-image.png"), size, bfs_graph.final_path)
#except:
#    print("no solution")
#try:
    #remake_image("{}{}".format(".\images\discretized\ ".strip(), image_name),
             #"{}{}{}".format("./images/final-image/astar1-node-", 1, "-image.png"),
             #size, a_star_1_graph.final_path)
#except:
#    print("no solution")
#try:
    #remake_image("{}{}".format(".\images\discretized\ ".strip(), image_name),
             #"{}{}{}".format("./images/final-image/astar2-node-", 1, "-image.png"), size,
             #a_star_2_graph.final_path)
#except:
#    print("no solution")
