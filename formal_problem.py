

class FormalProblem:

    def __init__(self, matrix):
        self.matrix = matrix
        self.goal_states = []
        for key, x in enumerate(matrix):
            x = x.tolist()
            indexes = [i for i, j in enumerate(x) if j == -1000]
            for index in indexes:
                self.goal_states.append([key, index])

            if 0 in x:
                self.initial_states = [key, x.index(0)]

    def actions(self, state):
        action_list = []
        # UPWARD
        if state[1] - 1 >= 0 and self.matrix[state[0], state[1] - 1] != 1000:
            action_list.append("LEFTY")
        # DOWNWARD
        if state[1] + 1 <= self.matrix.shape[1] - 1 and self.matrix[state[0], state[1] + 1] != 1000:
            action_list.append("RIGHTY")
        # RIGHTY
        if state[0] + 1 <= self.matrix.shape[0] - 1 and self.matrix[state[0] + 1, state[1]] != 1000:
            action_list.append("DOWNWARD")
        # LEFTY
        if state[0] - 1 >= 0 and self.matrix[state[0] - 1, state[1]] != 1000:
            action_list.append("UPWARD")
        return action_list

    def result(self, state, action):

        dictionary = {"RIGHTY": lambda: [state[0], state[1] + 1],
                      "LEFTY": lambda: [state[0], state[1] - 1],
                      "DOWNWARD": lambda: [state[0] + 1, state[1]],
                      "UPWARD": lambda: [state[0] - 1, state[1]]}
        return [dictionary[action]()]

    def goal_test(self, state):
        if state in self.goal_states or state == self.goal_states:
            return True
        return False

    def step_cost(self, state, action, next_state):
        return self.matrix[state] + self.matrix[next_state]

    def path_cost(self, list_states):
        cost = 0
        for x in list_states:
            cost = cost + self.matrix[x]
        return cost
