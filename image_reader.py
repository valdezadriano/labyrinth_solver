import image_slicer
import numpy
import scipy.stats
from PIL import Image
from math import ceil, sqrt


class ImageRead:

    # 1000 is black
    # 1 is white
    # 0 is green
    # -1000 is red

    def __init__(self, size, entry_image):
        self.size = size
        self.image_name = entry_image
        self.image = image_slicer.slice("{}{}".format(".\images\sliced_images\ ".strip(), entry_image), size)
        columns, rows = get_dimensions(self.size)
        self.discretized_matrix = numpy.empty((rows, columns), dtype=int)
        self.red_check = False
        self.greens_pos = [[-1, -1], [-1, -1]]

    def discretize(self):
        for tile in self.image:

            array = numpy.array(tile.image.getdata())
            mode = scipy.stats.mode(array)
            numpy.delete(array, 4)
            
            if(array == [0, 255, 0]).all(1).any() and (self.greens_pos[0] == [-1, -1] or
                                                       self.greens_pos[1] == [-1, -1]):
                if self.greens_pos[0] == [-1, -1]:
                    self.greens_pos[0] = [tile.column - 1, tile.row - 1]
                    mode = [0, 255, 0]
                    self.discretized_matrix[tile.column - 1, tile.row - 1] = -1000
                elif abs(self.greens_pos[0][0] - tile.column) > 7 or abs(self.greens_pos[0][1] - tile.row) > 7:
                    self.greens_pos[1] = [tile.column - 1, tile.row - 1]
                    mode = [0, 255, 0]
                    self.discretized_matrix[tile.column - 1, tile.row - 1] = -1000
                else:
                    mode = [255, 255, 255]
                    self.discretized_matrix[tile.column - 1, tile.row - 1] = 1

            elif ((array == [254, 0, 0]).all(1).any() or (array == [255, 0, 0]).all(1).any()) and\
                    self.red_check is False:
                mode = [255, 0, 0]
                self.discretized_matrix[tile.column - 1, tile.row - 1] = 0
                self.red_check = True
            else:

                if [mode[0].item(0), mode[0].item(1), mode[0].item(2)] == [0, 0, 0]:
                    mode = mode[0][0]
                    self.discretized_matrix[tile.column - 1, tile.row - 1] = 1000
                else:
                    mode = [255, 255, 255]
                    self.discretized_matrix[tile.column - 1, tile.row - 1] = 1

            new_image = Image.new('RGB', (tile.image.width, tile.image.height), tuple(mode))
            tile.image.paste(new_image)

    def join_images(self):
        joined_image = image_slicer.join(self.image)
        joined_image.save("{}{}".format("./images/discretized/", self.image_name))


def remake_image(entry_image_name, final_image_name, size, path):
    images = image_slicer.slice(entry_image_name, size)

    for tile in images:
        array = numpy.array(tile.image.getdata())
        mode = scipy.stats.mode(array)
        rgb = [mode[0].item(0), mode[0].item(1), mode[0].item(2)]
        if[tile.column - 1, tile.row - 1] in path[0] and rgb != [0, 255, 0] and rgb != [255, 0, 0]:

            new_image = Image.new('RGB', (tile.image.width, tile.image.height), tuple([170, 255, 255]))
            tile.image.paste(new_image)
        joined_image = image_slicer.join(images)
        joined_image.save(final_image_name)


def get_dimensions(n):
    columns = int(ceil(sqrt(n)))
    rows = int(ceil(n / float(columns)))
    return columns, rows
